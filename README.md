# Neo4jQueryBuilder

Neo4jQueryBuilder is a TypeScript module designed to simplify the process of building and executing Cypher queries for Neo4j using Deno. It provides a fluent interface for constructing queries and encapsulates the connection management with Neo4j.

## Features

- Singleton design pattern for efficient resource management.
- Fluent API for building Cypher queries.
- Query validation before execution.
- Easy integration with Deno.

## Installation

Simply import the `Neo4jQueryBuilder` class into your Deno project.

```typescript
import { Neo4jQueryBuilder } from "https://gitlab.com/elainejackson/neo4jquerybuilder/raw/main/mod.ts";
```

## Usage
**Creating a Singleton Instance**

Create a singleton instance of `Neo4jQueryBuilder`.

```typescript
const queryBuilder = Neo4jQueryBuilder.getInstance();
```

## Building and Executing Queries

Build and execute queries using a fluent interface.

**Creating a Node**
```typescript
const queryContext = queryBuilder.query()
  .create("person", "Person", { name: "Alice", age: 30 })
  .return(["person"]);

const results = await queryContext.execute();
```

**Updating a Node**
```typescript
await queryBuilder.query()
  .match("person", "Person")
  .where("person.name = $name", { name: "Alice" })
  .set("person", { age: 35 })
  .execute();
```